{strip}
<footer id="oe_footer" class="cf">
	<div class="footer-left">
		<small class="copyright">Copyright &copy; <a rel="external" href="{cms_copyright_link}">{sitename}&trade; {cms_version} - {cms_versionname}</a> - All Rights Reserved</small>
	</div>
	<div class="footer-right cf">
		<ul class="links">
			<li>
				<a href="http://www.idev.com.vn/" rel="external" title="{'security'|lang}">{'security'|lang}&trade;</a>
			</li>
		</ul>
	</div>
</footer>
{/strip}