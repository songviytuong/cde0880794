<h3>{$mod->Lang('edit_options')}</h3>
<h4>{$product.product_name}&nbsp;({$product.id})</h4>
{$langList}<br/><br/>
<script type="text/javascript">
    $(document).ready(function(){
    // move the skel row to a hidden div.
    var e = $('<div style="display: none;"></div>');
    $('body').append(e);
    var skel = $('#skel_row');
    skel.remove();
    e.append(skel);

    $('#timelines_table > tbody').sortable({
        cursor: 'move'
    });
    
    $('#add_option').click(function () {
        var e2 = skel.clone();
        e2.removeAttr('id').show();
        $('#timelines_table > tbody').append(e2);
    });
    
    $('.copy_option').live('click',function(){
    // find the preceding row.. and copy it.
    var tr = $(this).closest('tr');
    var clone = tr.clone();
        tr.after(clone);
    });
    
    $('.del_option').live('click', function () {
        var tbl = $(this).closest('table');
        var len = $('tbody tr', tbl).length;
        $(this).closest('tr').remove();
    });
    
    $('.edit_description').live('click',function(){
        var a = $(this).closest('tr');
        var b = $('.notes',a);
        var idx = a.prevAll().length;
        $('#notes_idx').val(idx);
        $('#notes_txt').val(b.val());
        $('#notes_dialog').dialog({
            modal: true,
            width: 'auto',
            buttons: [
                { text: "{$mod->Lang('ok')}",
                    click: function(){
                    var txt = $('#notes_txt').val();
                            var idx = $('#notes_idx').val();
                            $('#timelines_table tbody tr').eq(idx).find('.notes').val(txt);
                            $(this).dialog('close');
                        }
                    }
                ]
            });
    });

    $(document).on('click','#submit',function(){
    // don't remove the required attributes.
    var l = $('#timelines_table tbody tr').length;
    if( l == 0) {
            // if the text is empty, sku is empty, and the adjustment and qoh are 0
            // the we will delete the row and there will be no options
            // otherwise, require two rows.
            var row = $('#timelines_table > tbody > tr:first');
            var title = $('input.option_title', row).val();
            if (title == '') {
                // empty row
                row.remove();
            }
            else {
                alert('{$mod->Lang('error_needtworows')}');
                return false;
            }
        }
    });

    $(document).on('click','#cancel',function(){
        // cancel, remove required attributes
        $(':input').removeAttr('required');
    });

    $(document).on('click','#copyattribs',function () {
        // copy from something else.
        if (confirm('{$mod->Lang('confirm_copyoptions')}')) {
            $(':input').removeAttr('required');
            return true;
        }
        return false;
    });
});
</script>

<div id="notes_dialog" style="display: none;" title="{$mod->Lang('edit_option_notes')}">
    <div class="pageoverflow">
        <p class="pagetext">{$mod->Lang('notes')}:</p>
        <p class="pageinput">
            <input type="hidden" id="notes_idx" value=""/>
            <textarea id="notes_txt" rows="5" columns="40"></textarea>
        </p>
    </div>
</div>
<div class="information" style="display: block;">{$mod->Lang('info_edit_product_options')}</div>
<div id="warn_edit_product_options" class="pagewarning" style="display: block;">{$mod->Lang('warn_edit_product_options')}</div>

<div class="pageoptions">
    <a id="add_option">{cgimage image='icons/system/newobject.gif' alt=$mod->Lang('addoption') class='systemicon'} {$mod->Lang('addoption')}</a>
</div>
{$formstart}
<table class="pagetable" cellspacing="0" id="timelines_table">
    <thead>
        <tr>
            <th><span title="{$mod->Lang('title_title')}">*{$mod->Lang('title_title')}</span></th>
            <th><span title="{$mod->Lang('title_description')}">{$mod->Lang('title_description')}</span></th>
            <th><span title="{$mod->Lang('title_length')}">{$mod->Lang('title_length')}</span></th>
            <th class="pageicon"></th>
            <th class="pageicon"></th>
            <th class="pageicon"></th>
        </tr>
    </thead>
    <tbody>
        <tr id="skel_row" style="display: none;">
            <td><input type="text" class="option_title" name="{$actionid}title{$mleblock}[]" value="{$skel_row.title}" maxlength="255" required="required" placeholder="{$mod->Lang('ph_optiontext')}"/></td>
            <td>{cge_textarea wysiwyg=1 prefix=$actionid name="{$actionid}description{$mleblock}[]" rows=3 content="{$skel_row.description}"}</td>
            <td>{cgimage image='icons/system/edit.gif' class="edit_description" alt=$mod->Lang('edit_description')}</td>
            <td>{cgimage image='icons/system/copy.gif' class="copy_option" alt=$mod->Lang('copy_attribute')}
            <td>{cgimage image='icons/system/delete.gif' class="del_option" alt=$mod->Lang('delete_attribute')}
        </tr>
        {foreach from=$timelines name='timeline' item='timeline'}
            {assign var="title" value="title$mleblock"}
            {assign var="description" value="description$mleblock"}
            <tr id="timelines_{$timeline.id}">
                <td><input type="text" class="option_title" name="{$actionid}title{$mleblock}[]" value="{$timeline.$title}" maxlength="255" required="required" placeholder="{$mod->Lang('ph_optiontext')}"/>
                    <input type="hidden" class="option_id" name="{$actionid}ids[]" value="{$timeline.id}" />
                    <select id="timelines_with_vehicles" name="{$actionid}vehicle[]">
                        {html_options options=$timelines_with_vehicles selected=$timeline.vehicle}
                    </select>
                </td>
                <td>{cge_textarea wysiwyg="1" prefix="{$actionid}" name="description{$mleblock}[]" rows=3 content="{$timeline.$description}"}</td>
                <td><input type="text" class="option_meals" name="{$actionid}meals[]" value="{$timeline.meals}" /></td>
                <td>{cgimage image='icons/system/edit.gif' class="edit_description" alt=$mod->Lang('edit_description')}</td>
                <td>{cgimage image='icons/system/copy.gif' class="copy_option" alt=$mod->Lang('copy_attribute')}
                <td>{cgimage image='icons/system/delete.gif' class="del_option" alt=$mod->Lang('delete_attribute')}
            </tr>
        {/foreach}
        {for $foo=0 to 3 max=3}
            <tr id="timelines_">
                <td><input type="text" class="option_title" name="{$actionid}title{$mleblock}[]" value="" maxlength="255" placeholder="{$mod->Lang('ph_optiontext')}"/>
                    <input type="hidden" class="option_id" name="{$actionid}ids[]" value="" />
                    <select id="timelines_with_vehicles" name="{$actionid}vehicle[]">
                        {html_options options=$timelines_with_vehicles}
                    </select>
                </td>
                <td>{cge_textarea wysiwyg=1 prefix=$actionid name="description{$mleblock}[]" rows=3 content=""}</td>
                <td><input type="text" class="option_meals" name="{$actionid}meals[]" value="" /></td>
                <td>{cgimage image='icons/system/edit.gif' class="edit_description" alt=$mod->Lang('edit_description')}</td>
                <td>{cgimage image='icons/system/copy.gif' class="copy_option" alt=$mod->Lang('copy_attribute')}
                <td>{cgimage image='icons/system/delete.gif' class="del_option" alt=$mod->Lang('delete_attribute')}
            </tr>
        {/for}
    </tbody>
</table>
<div class="pageoptions">
    {if isset($products_with_timelines)}
        <label for="copy_attribs_from">{$mod->Lang('copy_options_from')}:</label>&nbsp;
        <select id="copy_attribs_from" name="{$actionid}copyfrom">
            {html_options options=$products_with_timelines}
        </select>&nbsp;
        <input type="submit" id="copyattribs" name="{$actionid}copyattribs" value="{$mod->Lang('ok')}"/>
    {/if}
</div>

<div class="pageoverflow">
    <p class="pagetext"></p>
    <p class="pageinput">
        <input type="submit" name="{$actionid}submit" id="submit" value="{$mod->Lang('submit')}"/>
        <input type="submit" name="{$actionid}apply" id="apply" value="{$mod->Lang('apply')}"/>
        <input type="submit" name="{$actionid}cancel" id="cancel" value="{$mod->Lang('cancel')}"/>
    </p>
</div>
{$formend}